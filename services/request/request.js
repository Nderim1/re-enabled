// (function(){
//   "use strict";
//   var fs = require('fs'),
//     path = require('path'),
//     certFile = path.resolve(__dirname, 'ssl/client.crt'),
//     keyFile = path.resolve(__dirname, 'ssl/client.key'),
//     caFile = path.resolve(__dirname, 'ssl/ca.cert.pem'),
//     request = require('request'),
//     srv = {};
//
//     var options = {
//       url: 'https://www.hydrofly.co.uk',
//       cert: fs.readFileSync(certFile),
//       key: fs.readFileSync(keyFile),
//       passphrase: 'password',
//       ca: fs.readFileSync(caFile)
//     };
//
//     srv.get = function(callback) {
//       request.get(options, callback);
//     };
//
//     srv.post = function(data, callback) {
//       request.post(options, data, callback);
//     };
//
//     module.export = srv;
// })();
