(function() {
  "use strict";
  var express = require('express');
  var router = express.Router();
  var http = require('http');


  router.route('/').get(function(req, resp) {
    http.get('http://hydrofly.co.uk/products', function(res) {
      res
        .on("data", function(chunk) {
          resp.status(res.statusCode).send(chunk);
        })
        .on("error", function(e) {
          resp.status(res.statusCode).send(e);
        });
    });
  });

  module.exports = router;
})();
