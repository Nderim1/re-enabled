(function() {
  "use strict";
  var express = require('express'),
    router = express.Router(),
    bodyParser = require('body-parser'),
    jsonParser = bodyParser.json(),
    request = require('request');

  router.route('/login').post(jsonParser, function(req, resp) {

    var options = {
      uri: 'http://localhost:8080/auth/login/admin',
      method: 'POST',
      json: req.body
    }

    request(options, function(e, r, body) {
      if (r) {
        resp.status(r.statusCode).send(r.body);
      } else if (e) {
        resp.status(500).send(e);
      };
    });

  });

  module.exports = router;

})();
