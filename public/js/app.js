(function() {
  "use strict"
  angular.module("hydroflyCMS", ['ui.router', 'ui.bootstrap', 'ngCookies', 'ngMap', 'angularCharts', 'ui-notification'])
    .run(function($cookies, authService, $timeout) {
      let user = $cookies.getObject('auth');
      if (user) {
        console.log("user is logged in " + user);
        $timeout(function() {
          authService.setUserdata(user);
        }, 100);
      };
    })
    .config(function(NotificationProvider){
       NotificationProvider.setOptions({
            delay: 10000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'left',
            positionY: 'bottom'
        });
    })
})();
