angular.module("hydroflyCMS")
  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: '/templates/pages/login.html',
        controller: 'loginCtrl'
      })
      .state('orders', {
        url: '/orders',
        templateUrl: '/templates/pages/orders.html',
        controller: 'ordersCtrl'
      })
      .state('products', {
        url: '/products',
        templateUrl: '/templates/pages/products.html',
        controller: 'productsCtrl'
      })
      .state('details/:id', {
        url: '/details/:id',
        templateUrl: '/templates/pages/details.html',
        controller: 'detailsCtrl'
      })
      .state('home', {
        url: '/home',
        templateUrl: '/templates/pages/home.html',
        controller: 'homeCtrl'
      });

    $urlRouterProvider.otherwise('/login');
  }]);
