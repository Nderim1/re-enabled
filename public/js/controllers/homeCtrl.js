(function() {
  "use strict"
  angular.module("hydroflyCMS")
    .controller("homeCtrl", ['$cookies', '$scope', '$state' ,'authService', '$location', 'Notification', function($cookies, $scope, $state, authService, $location, Notification) {

      $scope.userData = {};

      $scope.$on("USER_DATA_CHANGED", function(event, obj) {
        $scope.userData = obj;
      });

      $scope.auth = function() {
        if (!$cookies.getObject('auth')) {
          $location.path('/login');
        } else {
          $scope.userData = authService.getUserData();
        };
      };

      $scope.showDetails = function(id){
          
         $state.go('details/:id', {id: id})
     }
     
     $scope.changedClass = 'label label-success'
     $scope.changedStatus = 'OK'
     $scope.dangerClass = ''
     $scope.buttonClass = 'btn btn-primary'

     setTimeout(function() {
        Notification.error({message: 'ALARM! One of your patients is in danger!'});
       $scope.changedClass = 'label label-danger'
        $scope.changedStatus = 'Danger'
        $scope.dangerClass ='danger'
         $scope.buttonClass = 'btn btn-danger'
        $scope.$apply()
     }, 4000);

    }]);
})();
