(function() {
  "use strict"
  angular.module("hydroflyCMS")
    .controller("productsCtrl", ['$cookies', '$scope', '$http', 'authService', 'modalFactory', '$location', function($cookies, $scope, $http, authService, modalFactory, $location) {

      $scope.products = {};

      $scope.openModal = function(type, data) {
        modalFactory.generate(type, 'productsCtrl', data);
      };

      $scope.auth = function() {
        if (!$cookies.getObject('auth')) {
          $location.path('/login');
        } else {
          _getProducts();
        };
      };

      var _getProducts = function() {
        $http({
          method: 'GET',
          url: '/products'
        }).then(function(resp) {
          $scope.products = resp.data;
        }, function(error) {
          console.log(error);
        });
      };

    }]);
})();
