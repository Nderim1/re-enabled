(function() {
  "use strict"
  angular.module("hydroflyCMS")
    .controller("ordersCtrl", ['$scope', '$http', 'authService', '$location', '$cookies', function($scope, $http, authService, $location, $cookies) {

      $scope.orders = {};

      $scope.auth = function() {
        if(!$cookies.getObject('auth')) {
          $location.path('/login');
        } else {
          _getOrders();
        };
      };

      var _getOrders = function() {
        $http({
          method: 'GET',
          url: '/orders'
        }).then(function(resp) {
          $scope.orders = resp.data;
        }, function(error) {
          console.log(error);
        });
      };

    }]);
})();
