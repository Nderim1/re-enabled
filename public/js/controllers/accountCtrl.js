(function() {
  "use strict"
  angular.module('hydroflyCMS')
    .controller('accountCtrl', ['$scope', '$http', 'authService', '$timeout', '$cookies', function($scope, $http, authService, $timeout, $cookies) {

      $scope.userData = {};

      //load user data from authService + addresses + orders
      $timeout(function() {
        $scope.userData = authService.getUserData();
        authService.getAddresses();
        authService.getOrders();
      }, 100);
      //update userData when auth emits the USER_DATA_CHANGED event
      $scope.$on("USER_DATA_CHANGED", function(event, obj) {
        $scope.userData = obj;
      });

      /*===ADDRESSES MANAGMENT FUNCTIONS */
      $scope.newAddress = {};
      $scope.editing = false;
      $scope.selectedIndex = null;

      /* check variables */
      $scope.success = false;
      $scope.failed = false;

      $scope.deleteAddress = function() {
        $http({
          method: 'DELETE',
          url: '/user/addresses'
        }).then(function(resp){
          authService.getAddresses();
        }, function(error){
          $scope.error = "There was an error processing your request, please try again later";
        });

      };


      $scope.edit = function(address, index) {
        $scope.editing = !$scope.editing;
        $scope.selectedIndex = index;
        $scope.newAddress = address;
      };

      $scope.confirmEdit = function() {
        $http({
          method: 'POST',
          url: '/user/addresses',
          data: $scope.newAddress
        }).then(function(resp) {
          //show success message
          $scope.success = true;
          $setTimeout(function() {
            $scope.success = false;
          }, 1000);
          //reload the addresses
          //_getAddresses();
          //probably we will have to emit the USER_DATA_CHANGED event to get the updates shown on the webpage
        }, function(error) {
          $scope.failed = true;
          $setTimeout(function() {
            $scope.failed = false;
          }, 1000);
        });
        //hide the edit form
        $scope.editing = !$scope.editing;
      };

      /* ==== CREDENTIALS MANAGMENT FUNCTIONS === */
      $scope.ch_email = false;
      $scope.ch_psw = false;

      //TRIGGERS
      $scope.triggerPsw = function() {
        $scope.ch_psw = !$scope.ch_psw;
      }
      $scope.triggerEmail = function() {
        $scope.ch_email = !$scope.ch_email;
      }

      $scope.newEmail = {
        data: {
          email: '',
          email2: '',
          password: '',
          emailChanged: false,
          emailError: {
            status: false,
            message: ''
          }
        }
      };

      $scope.newPsw = {
        data: {
          password: '',
          password2: '',
          oldPassword: '',
          passwordChanged: false,
          passwordError: {
            status: false,
            message: ''
          }
        }
      };

      $scope.changeEmail = function(isValid) {
        $http({
          url: '/user/credentials/email?id=' + $scope.userData.id + '&token=' + $scope.userData.accessToken,
          method: 'POST',
          data: $scope.newEmail,
        }).then(function(resp) {
          $scope.email_changed = true;
          $scope.userData.email = resp.email;
          $rootScope.$broadcast("USER_DATA_CHANGED", userData);
          $cookies.put('user', userData);
        }, function(err) {
          $scope.email_error.status = true;
          $scope.email_error.message = err;
        });
      };

      $scope.changePsw = function(isValid) {
        $http({
          url: '/auth/changePsw',
          method: 'POST',
          data: {},
        }).then(function(resp) {
          $scope.psw_changed = true;
        }, function(err) {
          $scope.psw_error.status = true;
          $scope.psw_error.message = err;
        });
      };




      /*====REGISTER HYDROFLY===== */
      $scope.hydroflySerial = '';


      /* ====MODAL INSTANCE === */
      // $scope.closeModal = function(){
      //   $uibModalInstance.close();
      // };
    }]);
})();
