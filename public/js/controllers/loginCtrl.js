(function() {
  "use strict";
  angular.module('hydroflyCMS')
    .controller('loginCtrl', ['$scope', 'authService', '$location', '$timeout', '$rootScope', '$cookies', function($scope, authService, $location, $timeout, $rootScope, $cookies) {

      $scope.auth = function() {
        if ($cookies.getObject('auth')) {
          $location.path('/home');
        }
      };

      $scope.credentials = {
        email: "",
        password: ""
      };

      $scope.userData = {};

      $scope.$on("USER_DATA_CHANGED", function(event, obj) {
        $scope.userData = obj;
      });
      $scope.valid = false;
      $scope.submitted = false;
      $scope.loginError = {
        status: false,
        message: '',
        code: ''
      };

      //login function
      //*PARAMS*:
      //type: the type of login to perform
      //isValid: boolean, only for 'db' type logins, checks the validity of the form
      //		   before submitting
      $scope.login = function(isValid) {
        $scope.submitted = true;
        if (isValid) {
          $scope.valid = true;
          authService.login($scope.credentials, function(error, resp) {
            //reset user credentials
            $scope.credentials = {
              password: ""
            };
            if (error) {
              $scope.submitted = false;
              $scope.loginError.status = true;
              $scope.loginError.message = error.data.message ? error.data.message : "There server faced a problem elaborating the response, try again later";
              $scope.loginError.code = error.data.code;
              if (error.data.code == 100) {
                $scope.credentials.email = error.data.email;
              }
            }
            if (resp) {              
              $location.path('/home');
            }
          });
        }
      };

      $scope.logout = function() {
        authService.logout();
        console.log(JSON.stringify(authService.getUserData()));
      };

    }]);
})();
