angular.module('hydroflyCMS')
  .factory('modalFactory', ['$uibModal', '$rootScope', function($uibModal, $rootScope) {

    var modal = {};

    modal.generate = function(type, controller, data) {    
      var scope = $rootScope.$new();
      scope.data = data;
      var modalInstance = $uibModal.open({
        animation: true,
        scope: scope,
        templateUrl: '/templates/modals/' + type + '.html',
        size: 'lg'
      });
    };

    return modal;
  }]);
