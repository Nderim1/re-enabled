(function() {
  //the website offers the opportunity to login via different providers
  //FACEBOOK, TWITTER, LINKEDIN etc...
  //every login service writes over  shared variables held by the authFactory singleton.
  angular.module('hydroflyCMS')
    .factory('authService', ['$http', '$rootScope', '$cookies', '$location', '$window', function($http, $rootScope, $cookies, $location, $window) {

      var auth = {};

      var userData = {
        id: '',
        name: '',
        email: '',
        loggedIn: false
      };

      var _createCookie = function(value) {
        //var expDate = new Date() + 7;
        $cookies.putObject('auth', value);
      };

      auth.login = function(credentials, callback) {
        // $http({
        //   method: 'POST',
        //   url: '/auth/login',
        //   data: credentials,
        // }).then(
        //   function(resp) {
        //     userData.id = resp.data.id;
        //     userData.email = resp.data.email;
        //     userData.name = resp.data.name;
        //     userData.loggedIn = true;
        //     _createCookie(userData);
        //     $rootScope.$broadcast("USER_DATA_CHANGED", userData);
        //     callback(null, true);
        //   },
        //   function(error) {
        //     console.log(error);
        //     callback(error, null);
        //   });
        if(credentials.email == "assistente@associazione.it" && credentials.password == "password") {
          userData.id = "12345";
          userData.email = "assistente@associazione.it";
          userData.name = "Agente";
          userData.loggedIn = true;
          _createCookie(userData);
          $rootScope.$broadcast("USER_DATA_CHANGED", userData);
          callback(null, true);
        }
      };

      auth.logout = function(callback) {
        $cookies.remove('auth');
        console.log($cookies.getObject('auth'));
        $location.path('/login');
        $window.location.reload();
      };

      auth.getUserData = function() {
        return userData;
      };

      auth.setUserdata = function(user) {
        userData = user;
        $rootScope.$broadcast("USER_DATA_CHANGED", userData);
      };

      return auth;
    }]);
})();
