(function() {
  "use strict";
  var express = require('express'),
    app = express(),
    port = process.env.PORT || 5651,
    auth = require('./routes/auth'),
    products = require('./routes/products'),
    orders = require('./routes/orders');

  app.use(express.static(__dirname + '/public'));
  app.use('/auth', auth);
  app.use('/products', products);
  app.use('/orders', orders);

  app.get('/', function(req, resp) {
    resp.sendFile(__dirname + '/public/index.html');
  });

  app.listen(port, function() {
    console.log('listening on port ' + port);
  });
})();
